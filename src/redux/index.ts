import { applyMiddleware, compose, createStore } from 'redux';
import { persistCombineReducers, persistStore } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import reduxSaga from 'redux-saga';

import reducers from './reducers';
import sagas from './sagas';

const persistConfig = {
  key: 'root',
  storage,
  // whitelist: [],
  // transforms: [],
};

const combinedReducers = persistCombineReducers(persistConfig, reducers);

const configureStore = () => {
  const sagaMiddleware = reduxSaga();
  const enhancer = compose(
    applyMiddleware(sagaMiddleware),
    global.reduxNativeDevTools ? global.reduxNativeDevTools({ name: 'Pandoria' }) : nope => nope,
  );
  const store = createStore(combinedReducers, enhancer);
  const persistor = persistStore(store);
  sagaMiddleware.run(sagas);
  return { store, persistor };
};

export const { store, persistor } = configureStore();
