import { MapCreators, MapTypes } from './map/actions';

export const AllTypes = {
  ...MapTypes,
};

export const AllCreators = {
  ...MapCreators,
};
