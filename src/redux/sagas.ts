// import R from 'ramda';
import { all } from 'redux-saga/effects';

// import type { Saga } from '~types';

// import auth from './auth/sagas';

const allSagas = [];
// R.flatten([
//   // auth,
// ]);

export default function* sagas() {
  yield all(allSagas);
}
