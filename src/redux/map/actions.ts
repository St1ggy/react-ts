import { Hexes } from './types';

export const MapTypes = {
  MapSaveHexes: 'MapSaveHexes',
};

interface ISaveHexes {
  type: 'MapSaveHexes';
  hexes: Hexes;
}
const mapSaveHexes = (hexes: Hexes): ISaveHexes => ({
  type: 'MapSaveHexes',
  hexes,
});

export type LocalReduxAction = ISaveHexes;

export const MapCreators = {
  mapSaveHexes,
};
