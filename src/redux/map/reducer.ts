import R from 'ramda';
import { MapTypes } from './actions';
import { Hexes } from './types';

interface IMapState {
  hexes: Hexes;
}

export const initialState: IMapState = {
  hexes: [1, 2, 3],
};

const mapSaveHexes = (state, { hexes }) => R.assoc('hexes', hexes, state);

const reducer = (state: IMapState = initialState, action): IMapState => {
  if (!action || !action.type) return state;
  switch (action.type) {
    case MapTypes.MapSaveHexes:
      return mapSaveHexes(state, action);
    default:
      return state;
  }
};

export default reducer;
