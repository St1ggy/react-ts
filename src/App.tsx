import R from 'ramda';
import React from 'react';
import { connect } from 'react-redux';

interface IConnectedProps {
  hexes: any[];
}

type Props = IConnectedProps;

const Component = ({ hexes }: Props) => {
  return (
    <div>
      <h1>Here is Map</h1>
      {hexes.map(x => (
        <h3 key={`hex-${x}`}>{x}</h3>
      ))}
    </div>
  );
};

const MSTP = ({ map: { hexes } }) => ({ hexes });

const ConnectedComponent = connect(MSTP)(Component);

export const App = () => {
  return (
    <div>
      <h5>before map</h5>
      <ConnectedComponent />
      <h5>after map</h5>
    </div>
  );
};
