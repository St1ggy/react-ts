import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';

import { css, cssExtract, cssMinify, devServer, jsUglify, pug } from './webpack.cfg';

const PATHS = {
  app: path.join(__dirname, 'app'),
  src: path.join(__dirname, 'src'),
};
const resolvePaths = (...args: string[]): string => {
  return path.resolve(__dirname, 'src', ...args);
};
const common: webpack.Configuration = merge(
  {
    devtool: 'source-map',
    entry: `${PATHS.src}/index`,
    module: {
      rules: [
        { test: /\.tsx?/, loader: 'awesome-typescript-loader' },
        // { test: /\.ts?$/, loader: "awesome-typescript-loader" },
        { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
      ],
    },
    output: {
      filename: 'js/app.js',
      path: PATHS.app,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: `${PATHS.src}/index.pug`,
      }),
    ],
    resolve: {
      alias: {
        // "~types": resolvePaths("types"),
        // "~utils": resolvePaths("utils"),
        '~redux': resolvePaths('redux'),
        '~root': resolvePaths('root'),
      },
      extensions: ['.ts', '.tsx', '.js', '.json'],
    },
    // externals: {
    //   react: "React",
    //   "react-dom": "ReactDOM"
    // }
    // optimization: {
    //   splitChunks: {
    //     chunks: "all"
    //   }
    // }
  },
  pug(),
);
export default env =>
  env === 'production'
    ? merge(common, { mode: 'production' }, cssExtract(), jsUglify(), cssMinify())
    : merge(common, { mode: 'development' }, css(), devServer());
