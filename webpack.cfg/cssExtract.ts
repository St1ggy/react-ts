import ExtractTextPlugin from 'extract-text-webpack-plugin';

export const cssExtract = () => {
  return {
    module: {
      rules: [
        {
          test: /\.styl$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            publicPath: '../',
            use: ['css-loader', 'stylus-loader'],
          }),
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader',
          }),
        },
      ],
    },
    plugins: [new ExtractTextPlugin('./css/[name].css')],
  };
};
