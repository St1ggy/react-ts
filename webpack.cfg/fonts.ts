export const fonts = () => ({
  module: {
    rules: [
      {
        loaders: ['style', 'css?importLoaders=1', 'font?format[]=truetype&format[]=woff&format[]=embedded-opentype'],
        test: /\.(woff|woff2|ttf|eot|otf)$/,
      },
    ],
  },
});
