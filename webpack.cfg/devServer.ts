export const devServer = () => ({
  devServer: {
    contentBase: './app',
    inline: true,
    open: true,
    overlay: true,
    port: 3000,
  },
});
