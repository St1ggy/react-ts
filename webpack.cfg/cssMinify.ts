import cssnano from 'cssnano';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';

export const cssMinify = () => ({
  plugins: [
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g,
      canPrint: true,
      cssProcessor: cssnano,
      cssProcessorOptions: { discardComments: { removeAll: true } },
    }),
  ],
});
