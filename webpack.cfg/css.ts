export const css = () => ({
  module: {
    rules: [
      {
        loaders: 'style-loader!css-loader!stylus-loader',
        test: /\.styl$/,
      },
      {
        loaders: 'style-loader!css-loader',
        test: /\.css$/,
      },
    ],
  },
});
