export const pug = () => ({
  module: {
    rules: [
      {
        loader: 'pug-loader',
        options: {
          pretty: true,
        },
        test: /\.pug$/,
      },
    ],
  },
});
